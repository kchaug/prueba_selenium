package com;

import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import junit.framework.AssertionFailedError;
import junit.framework.TestListener;
import org.junit.*;
import org.junit.Test;
import static org.junit.Assert.assertTrue;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import ru.yandex.qatools.ashot.AShot;
import ru.yandex.qatools.ashot.Screenshot;
import ru.yandex.qatools.ashot.shooting.ShootingStrategies;

import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.concurrent.TimeUnit;


public class MavenTest extends JUnitTestReporter {
    private static WebDriver Driver;

    public static String evidencia (String imagen) throws IOException {
        Screenshot imgEvidencia = new AShot().shootingStrategy(ShootingStrategies.viewportPasting(100)).takeScreenshot(Driver);
        String destino ="Reporte_evidencia/"+ imagen + ".png";
        //String destino = System.getProperty("user.dir")+"\\Reporte_evidencia\\"+ imagen + ".png";
        ImageIO.write(imgEvidencia.getImage(),"PNG",new File(destino));
        return destino;
    }

    @Before
    public void iniciarConexion() throws IOException {
        String host = "34.70.220.97";
        DesiredCapabilities dc;
        //System.setProperty("webdriver.chrome.driver", "./src/test/resources/chromedriver.exe");
        //Driver = new ChromeDriver();
        
        if(System.getProperty("BROWSER") != null &&
                System.getProperty("BROWSER").equalsIgnoreCase("firefox")){
                dc = DesiredCapabilities.firefox();
        }else{
            dc = DesiredCapabilities.chrome();
        }

        if(System.getProperty("HUB_HOST") != null){
            host = System.getProperty("HUB_HOST");
        }

        String completeUrl = "http://" + host + ":4444/wd/hub";
        this.Driver = new RemoteWebDriver(new URL(completeUrl), dc);

        Driver.manage().window().maximize();
        Driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
    }

    @Test
    public void prueba2() throws InterruptedException {
        test = extent.createTest("Prueba2");
        test.log(Status.INFO, MarkupHelper.createLabel("Inicia la prueba "+"Prueba2", ExtentColor.CYAN));
        test.log(Status.INFO, MarkupHelper.createLabel("Navega a la pagina", ExtentColor.CYAN));
            Driver.get("https://simple.ripley.cl/");
        test.log(Status.INFO, MarkupHelper.createLabel("Busqueda del moto g7", ExtentColor.CYAN));
            WebElement busqueda;
            busqueda = Driver.findElement(By.className("js-search-bar"));
            busqueda.sendKeys("Motorola moto g7");
        test.log(Status.INFO, MarkupHelper.createLabel("Click al moto g7", ExtentColor.CYAN));
            Driver.findElement(By.linkText("Motorola moto g7 64gb - negro")).click();
        test.log(Status.INFO, MarkupHelper.createLabel("Obtener el precio del moto g7", ExtentColor.CYAN));
            String precio = Driver.findElement(By.className("product-ripley-price")).getText();
            precio = precio.substring(15, 23);
        test.log(Status.INFO, MarkupHelper.createLabel("Click al boton de compra", ExtentColor.CYAN));
            Driver.findElement(By.id("buy-button")).click();
        test.log(Status.INFO, MarkupHelper.createLabel("Click al boton ir a la bolsa", ExtentColor.CYAN));
            Driver.findElement(By.linkText("Ir a la bolsa")).click();
        test.log(Status.INFO, MarkupHelper.createLabel("Obtener el precio en el carro de compra del moto g7", ExtentColor.CYAN));
            String precio_final = Driver.findElement(By.className("is-offer")).getText();
            precio_final = precio_final.substring(21, 29);
            precio_final="24erfwfwr";
            try {
                test.log(Status.INFO, MarkupHelper.createLabel("Comparación precio moto g7 y precio bolsa de compra", ExtentColor.CYAN));
                    assertTrue(precio_final.contentEquals(precio));
                    System.out.println("Carro de compra cargado exitosamente");
            }
        catch (AssertionFailedError e) {
            System.out.println("No se cargo el carro de compra");
        }
    }
    @Test
    public void prueba3() throws InterruptedException {
        test = extent.createTest("Prueba3");
        test.log(Status.INFO, MarkupHelper.createLabel("Inicia la prueba "+"Prueba3", ExtentColor.CYAN));
        test.log(Status.INFO, MarkupHelper.createLabel("Navega a la pagina", ExtentColor.CYAN));
            Driver.get("https://simple.ripley.cl/");
        test.log(Status.INFO, MarkupHelper.createLabel("Busqueda del moto g7", ExtentColor.CYAN));
            WebElement busqueda;
            busqueda = Driver.findElement(By.className("jws-search-bar"));
            busqueda.sendKeys("Motorola moto g7");
        test.log(Status.INFO, MarkupHelper.createLabel("Click al moto g7", ExtentColor.CYAN));
            Driver.findElement(By.linkText("Motorola moto g7 64gb - negro")).click();
        test.log(Status.INFO, MarkupHelper.createLabel("Click al boton de compra", ExtentColor.CYAN));
            Driver.findElement(By.id("buy-button")).click();
        test.log(Status.INFO, MarkupHelper.createLabel("Click al boton ir a la bolsa", ExtentColor.CYAN));
            Driver.findElement(By.linkText("Ir a la bolsa")).click();
        test.log(Status.INFO, MarkupHelper.createLabel("Eliminar producto", ExtentColor.CYAN));
            Driver.findElement(By.id("onesignal-popover-cancel-button")).click();
            Thread.sleep(15000);
            Driver.findElement(By.className("fa-trash")).click();
        String resultado = Driver.findElement(By.className("is-empty")).getText();
        try {
            test.log(Status.INFO, MarkupHelper.createLabel("Comprobación Eliminación de producto", ExtentColor.CYAN));
            assertTrue(resultado.contentEquals("Tu bolsa está vacía"));
        }
        catch (AssertionFailedError e) {
            System.out.println(resultado.contentEquals("Tu bolsa está vacía") ? "Carro de compra vaciado de forma exitosa" : "No se pudo vaciar el carro de compra");
        }
    }

    public void cerrarConexion(){
        Driver.close();
    }

}
