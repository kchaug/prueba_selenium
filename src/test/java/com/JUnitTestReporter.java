package com;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.ChartLocation;
import com.aventstack.extentreports.reporter.configuration.Theme;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.rules.TestRule;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class JUnitTestReporter {
    public static ExtentHtmlReporter reporter;
    public static ExtentReports extent;
    public static ExtentTest test;
    public static MavenTest objcs = new MavenTest();

    @BeforeClass
    public static void inicio() throws IOException {
        File f = new File("Reporte_evidencia");
        File [] archivos = f.listFiles();

        for (int x=0; x<archivos.length; x++)
        {
            archivos[x].delete();
        }

        reporter = new ExtentHtmlReporter("Reporte_evidencia/reporte.html");
        extent = new ExtentReports();
        extent.attachReporter(reporter);

        extent.setSystemInfo    ("OS.name", System.getProperty("os.name"));
        extent.setSystemInfo("Ambiente", "QA");
        extent.setSystemInfo("nombre de usuario", System.getProperty("user.name"));

        reporter.config().setChartVisibilityOnOpen(true);
        reporter.config().setDocumentTitle("Reporte de prueba");
        reporter.config().setReportName("Reporte de prueba");
        reporter.config().setTestViewChartLocation(ChartLocation.TOP);
        reporter.config().setTheme(Theme.DARK);
    }

    @Rule
    public TestRule junitWatcher = new TestWatcher() {

        @Override
        public Statement apply(Statement base, Description description) {
            return super.apply(base, description);
        }

        @Override
        protected void succeeded(Description description) {
           System.out.println(description.getDisplayName() + " "
                    + "Test pasado!");
            test.log(Status.PASS, MarkupHelper.createLabel(description.getDisplayName()+" Caso de prueba pasado", ExtentColor.GREEN));
            objcs.cerrarConexion();
        }

        @Override
        protected void failed(Throwable e, Description description) {
            System.out.println(description.getDisplayName() + " "
                    + e.getClass().getSimpleName());
                try {
                    Date objFecha = new Date();
                    String strDateFormat = "hh.mm.ss.a-dd-MM-yyyy";
                    SimpleDateFormat objSDF = new SimpleDateFormat(strDateFormat);
                    String obtImagen = MavenTest.evidencia("Evidencia"+"-"+objSDF.format(objFecha));
                    test.log(Status.FAIL, MarkupHelper.createLabel(description.getDisplayName()+" Caso de prueba Fallado debido a: "+ e.getClass().getSimpleName()+" " + test.addScreenCaptureFromPath(obtImagen), ExtentColor.RED));
                    objcs.cerrarConexion();
                }
                catch (Exception er)
                {
                    System.out.println(er);
                }
        }
    };
    @AfterClass
    public static void fin()
    {
        try{
            extent.flush();
        }
        catch (Exception e)
        {
            System.out.println("Ha ocurrido un error al guardar el reporte: " + e.getMessage());
        }

    }
}
