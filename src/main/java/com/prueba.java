package com;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.HttpClientBuilder;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;


public class prueba {

    private static URI JIRA_URL = URI.create("http://34.95.235.106:8100/");
    private static final String JIRA_ADMIN_USERNAME = "tsoft";
    private static final String JIRA_ADMIN_PASSWORD = "admin123";

    public static void main(String[] args) throws IOException, InterruptedException {
        WebDriver Driver;
        System.setProperty("webdriver.chrome.driver", "./src/main/resources/ChromeDriver/chromedriver.exe");
        Driver = new ChromeDriver();
        Driver.manage().window().maximize();

        Driver.get("https://simple.ripley.cl/");
        WebElement busqueda;

        busqueda = Driver.findElement(By.className("js-search-bar"));
        busqueda.sendKeys("Huawei mate 20 lite azul wom");
        Thread.sleep(3000);
        Driver.findElement(By.linkText("Huawei mate 20 lite azul wom")).click();
        Thread.sleep(10000);
        String precio = Driver.findElement(By.className("product-ripley-price")).getText();
        precio = precio.substring(15, 23);
        System.out.println(precio);
        Driver.findElement(By.id("buy-button")).click();
        Thread.sleep(10000);
        Driver.findElement(By.linkText("Ir a la bolsa")).click();
        Thread.sleep(10000);
        String precio_final = Driver.findElement(By.className("is-offer")).getText();
        precio_final = precio_final.substring(21, 29);
        System.out.println(precio_final);
        System.out.println(precio_final.contentEquals(precio)? "Carro de compra cargado exitosamente": "No se cargo el carro de compra");

        Driver.close();
    }

}
